# hermit-abi

This is small interface to call functions from the unikernel [RustyHermit](https://github.com/hermitcore/libhermit-rs).
It is used to build the target `x86_64-unknown-hermit`.

## License

Licensed under either of

* Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
* MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
